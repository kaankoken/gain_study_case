import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;
import 'package:gain_study_case/src/app.dart';
import 'package:gain_study_case/src/di/bloc_injector.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DotEnv.dotenv.load(fileName: ".env");
  await configureInjection();
  return runZonedGuarded(() async {
    runApp(App());
  }, (error, stack) {
    print(stack);
    print(error);
  });
}


/* 
  TODO: Splash Screen,
  TODO: Refactor
  TODO: Skeleton,
  TODO:  Unit Test -> OK
         Integration Test -> NOK
*/