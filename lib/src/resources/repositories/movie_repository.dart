import 'package:gain_study_case/src/models/state.dart';
import 'package:gain_study_case/src/resources/apis.dart';
import 'package:gain_study_case/src/resources/network_helper.dart';
import 'package:injectable/injectable.dart';

@injectable
class MovieRepository {
  final NetworkHelper _networkHelper;

  MovieRepository(this._networkHelper);

  Future<State> getMovieList({
    Map<String, String> pathParams,
    Map<String, String> queryParams,
  }) =>
      _networkHelper.sendRequest(
        RequestMethod.GET,
        APIs.POPULAR_MOVIES,
        pathParameters: pathParams,
        queryParameters: queryParams,
      );

  Future<State> getMovieDetail({
    Map<String, String> pathParams,
    Map<String, String> queryParams,
  }) =>
      _networkHelper.sendRequest(
        RequestMethod.GET,
        APIs.MOVIE_DETAIL,
        pathParameters: pathParams,
        queryParameters: queryParams,
      );

  Future<State> getLatestMovie({
    Map<String, String> pathParams,
    Map<String, String> queryParams,
  }) =>
      _networkHelper.sendRequest(
        RequestMethod.GET,
        APIs.LATEST_MOVIE,
        pathParameters: pathParams,
        queryParameters: queryParams,
      );

  Future<State> getMovieTrailers({
    Map<String, String> pathParams,
    Map<String, String> queryParams,
  }) =>
      _networkHelper.sendRequest(
        RequestMethod.GET,
        APIs.WATCH_MOVIE_TRAILER,
        pathParameters: pathParams,
        queryParameters: queryParams,
      );
}
