import 'package:gain_study_case/src/models/state.dart';
import 'package:gain_study_case/src/resources/apis.dart';
import 'package:gain_study_case/src/resources/network_helper.dart';
import 'package:injectable/injectable.dart';

@injectable
class TvShowRepository {
  final NetworkHelper _networkHelper;

  TvShowRepository(this._networkHelper);

  Future<State> getTvShowList({
    Map<String, String> pathParams,
    Map<String, String> queryParams,
  }) =>
      _networkHelper.sendRequest(
        RequestMethod.GET,
        APIs.POPULAR_TVSHOWS,
        pathParameters: pathParams,
        queryParameters: queryParams,
      );

  Future<State> getTvShowDetail({
    Map<String, String> pathParams,
    Map<String, String> queryParams,
  }) =>
      _networkHelper.sendRequest(
        RequestMethod.GET,
        APIs.TVSHOW_DETAIL,
        pathParameters: pathParams,
        queryParameters: queryParams,
      );

  Future<State> getLatestTvShow({
    Map<String, String> pathParams,
    Map<String, String> queryParams,
  }) =>
      _networkHelper.sendRequest(
        RequestMethod.GET,
        APIs.LATEST_TVSHOW,
        pathParameters: pathParams,
        queryParameters: queryParams,
      );

  Future<State> getTvSeriesTrailers({
    Map<String, String> pathParams,
    Map<String, String> queryParams,
  }) =>
      _networkHelper.sendRequest(
        RequestMethod.GET,
        APIs.WATCH_TV_SERIES_TRAILER,
        pathParameters: pathParams,
        queryParameters: queryParams,
      );
}
