import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;
import 'package:gain_study_case/src/models/state.dart';
import 'package:gain_study_case/src/resources/apis.dart';
import 'package:http/http.dart';
import 'package:injectable/injectable.dart';

@injectable
class NetworkHelper {
  final Client _client;
  final _apiKey = DotEnv.dotenv.env['MOVIEDB_API_KEY'];

  NetworkHelper(this._client);

  Future<State> sendRequest<T>(
    RequestMethod requestMethod,
    String apis, {
    Map<String, String> body,
    Map<String, String> pathParameters,
    Map<String, String> queryParameters,
  }) async {
    if (requestMethod == RequestMethod.GET) {
      return await _get(
        apis,
        body: body,
        pathParameters: pathParameters,
        queryParameters: queryParameters,
      );
    } else {
      // TODO: post
    }
  }

  Future<State> _get<T>(
    String apis, {
    Map<String, String> body,
    Map<String, String> pathParameters,
    Map<String, String> queryParameters,
  }) async {
    var uri;

    if (pathParameters != null) {
      uri = Uri.parse(_replacePathParameters(apis, pathParameters));
    } else {
      uri = Uri.parse(apis);
    }

    if (queryParameters == null) {
      queryParameters = Map<String, String>();
    }

    queryParameters["api_key"] = _apiKey;
    uri = uri.replace(queryParameters: queryParameters);

    try {
      final response = await _client.get(uri);
      print(response.body.toString());

      if (response.statusCode >= 200 && response.statusCode <= 202) {
        //var snapshot = ListDetailModel.fromJson(json.decode(response.body));
        return State<T>.success(json.decode(response.body));
      } else {
        return State<String>.error("${response.statusCode} - ${response.body}");
      }
    } on Exception catch (exception) {
      return State<String>.error(exception.toString());
    } catch (error) {
      return State<T>.error(error);
    }
  }

  String _replacePathParameters(
      String uri, Map<String, String> pathParameters) {
    String editedUri = uri.toString();
    pathParameters.forEach((key, value) {
      editedUri = editedUri.replaceFirst("{$key}", value);
    });

    return editedUri;
  }
}
