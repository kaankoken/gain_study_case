class APIs {
  static const _baseURL = "https://api.themoviedb.org/3";

  // Start Movie Region
  static const POPULAR_MOVIES = "$_baseURL/movie/popular";
  static const MOVIE_DETAIL = "$_baseURL/movie/{movie_id}";
  static const LATEST_MOVIE = "$_baseURL/movie/latest";
  static const WATCH_MOVIE_TRAILER = "$_baseURL//movie/{movie_id}/videos";
  // End Movie Region

  // Start Tv Series Region
  static const POPULAR_TVSHOWS = "$_baseURL/tv/popular";
  static const TVSHOW_DETAIL = "$_baseURL/tv/{tv_id}";
  static const LATEST_TVSHOW = "$_baseURL/tv/latest";
  static const WATCH_TV_SERIES_TRAILER = "$_baseURL/tv/{tv_id}/videos";
  // End Tv Series Region
}

enum RequestMethod { GET, POST }
