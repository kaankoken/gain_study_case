import 'package:gain_study_case/src/blocs/base/base_bloc.dart';
import 'package:gain_study_case/src/resources/repositories/movie_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@injectable
class MovieTrailerBloc extends BaseBloc {
  final MovieRepository _repository;
  BehaviorSubject<dynamic> _movieTrailerFetcher;

  MovieTrailerBloc(this._repository);

  Stream<dynamic> get fetchMovieTrailer => _movieTrailerFetcher.stream;
  getMovieTrailer(Map<String, String> pathParams) async {
    dynamic itemModel = await _repository.getMovieTrailers(
      pathParams: pathParams,
    );
    _movieTrailerFetcher.sink.add(itemModel.value);
  }

  @override
  init() {
    _movieTrailerFetcher = BehaviorSubject<dynamic>();
  }

  @override
  dispose() {
    _movieTrailerFetcher.close();
  }
}
