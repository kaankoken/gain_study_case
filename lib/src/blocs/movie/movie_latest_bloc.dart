import 'package:gain_study_case/src/blocs/base/base_bloc.dart';
import 'package:gain_study_case/src/resources/repositories/movie_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';

@injectable
class MovieLatestBloc extends BaseBloc {
  final MovieRepository _repository;
  PublishSubject<dynamic> _latestMovie;

  MovieLatestBloc(this._repository);

  Stream<dynamic> get fetchLatestMovie => _latestMovie.stream;
  getLatestMovie() async {
    dynamic itemModel = await _repository.getLatestMovie();
    _latestMovie.sink.add(itemModel.value);
  }

  @override
  init() {
    _latestMovie = PublishSubject<dynamic>();
  }

  @override
  dispose() {
    _latestMovie.close();
  }
}
