import 'package:gain_study_case/src/blocs/base/base_bloc.dart';
import 'package:gain_study_case/src/resources/repositories/movie_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@injectable
class MovieListBloc extends BaseBloc {
  final MovieRepository _repository;
  PublishSubject<dynamic> _movieListFetcher;

  MovieListBloc(this._repository);

  Stream<dynamic> get fetchMovieList => _movieListFetcher.stream;
  getMovieList(Map<String, String> queryString) async {
    dynamic itemModel = await _repository.getMovieList(
      queryParams: queryString,
    );

    try {
      _movieListFetcher.sink.add(itemModel.value);
    } catch (error) {}
  }

  @override
  init() {
    _movieListFetcher = PublishSubject<dynamic>();
  }

  @override
  dispose() {
    _movieListFetcher.close();
  }
}
