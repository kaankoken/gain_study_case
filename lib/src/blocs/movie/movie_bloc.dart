import 'package:gain_study_case/src/blocs/base/base_bloc.dart';
import 'package:gain_study_case/src/resources/repositories/movie_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@injectable
class MovieBloc extends BaseBloc {
  final MovieRepository _repository;
  BehaviorSubject<dynamic> _movieDetailFetcher;

  MovieBloc(this._repository);

  Stream<dynamic> get fetchMovieDetail => _movieDetailFetcher.stream;
  getMovieDetail(Map<String, String> pathParams) async {
    dynamic itemModel = await _repository.getMovieDetail(
      pathParams: pathParams,
    );
    _movieDetailFetcher.sink.add(itemModel.value);
  }

  @override
  init() {
    _movieDetailFetcher = BehaviorSubject<dynamic>();
  }

  @override
  dispose() {
    _movieDetailFetcher.close();
  }
}
