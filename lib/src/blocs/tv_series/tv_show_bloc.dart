import 'package:gain_study_case/src/blocs/base/base_bloc.dart';
import 'package:gain_study_case/src/resources/repositories/tv_show_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@injectable
class TvShowBloc extends BaseBloc {
  final TvShowRepository _repository;
  PublishSubject<dynamic> _tvShowDetailFetcher;

  TvShowBloc(this._repository);

  Stream<dynamic> get fetchTvShowDetail => _tvShowDetailFetcher.stream;
  getTvShowDetail(Map<String, String> pathParams) async {
    dynamic itemModel = await _repository.getTvShowDetail(
      pathParams: pathParams,
    );
    _tvShowDetailFetcher.sink.add(itemModel.value);
  }

  @override
  init() {
    _tvShowDetailFetcher = PublishSubject<dynamic>();
  }

  @override
  dispose() {
    _tvShowDetailFetcher.close();
  }
}
