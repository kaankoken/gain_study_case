import 'package:gain_study_case/src/blocs/base/base_bloc.dart';
import 'package:gain_study_case/src/resources/repositories/tv_show_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';

@injectable
class TvShowListBloc extends BaseBloc {
  final TvShowRepository _repository;
  PublishSubject<dynamic> _tvShowListFetcher;

  TvShowListBloc(this._repository);

  Stream<dynamic> get fetchTvShowList => _tvShowListFetcher.stream;
  getTvShowList(Map<String, String> queryString) async {
    dynamic itemModel = await _repository.getTvShowList(
      queryParams: queryString,
    );
    _tvShowListFetcher.sink.add(itemModel.value);
  }

  @override
  init() {
    _tvShowListFetcher = PublishSubject<dynamic>();
  }

  @override
  dispose() {
    _tvShowListFetcher.close();
  }
}
