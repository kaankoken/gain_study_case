import 'package:gain_study_case/src/blocs/base/base_bloc.dart';
import 'package:gain_study_case/src/resources/repositories/tv_show_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@injectable
class TvShowTrailerBloc extends BaseBloc {
  final TvShowRepository _repository;
  BehaviorSubject<dynamic> _tvShowTrailerFetcher;

  TvShowTrailerBloc(this._repository);

  Stream<dynamic> get fetchTvShowTrailer => _tvShowTrailerFetcher.stream;
  getTvSeriesTrailer(Map<String, String> pathParams) async {
    dynamic itemModel = await _repository.getTvSeriesTrailers(
      pathParams: pathParams,
    );
    _tvShowTrailerFetcher.sink.add(itemModel.value);
  }

  @override
  init() {
    _tvShowTrailerFetcher = BehaviorSubject<dynamic>();
  }

  @override
  dispose() {
    _tvShowTrailerFetcher.close();
  }
}
