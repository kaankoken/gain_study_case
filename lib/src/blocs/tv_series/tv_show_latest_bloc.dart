import 'package:gain_study_case/src/blocs/base/base_bloc.dart';
import 'package:gain_study_case/src/resources/repositories/tv_show_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@injectable
class TvShowLatestBloc extends BaseBloc {
  final TvShowRepository _repository;
  PublishSubject<dynamic> _latestTvShow;

  TvShowLatestBloc(this._repository);

  Stream<dynamic> get fetchLatestTvShow => _latestTvShow.stream;
  getLatestTvShow() async {
    dynamic itemModel = await _repository.getLatestTvShow();
    _latestTvShow.sink.add(itemModel.value);
  }

  @override
  init() {
    _latestTvShow = PublishSubject<dynamic>();
  }

  @override
  dispose() {
    _latestTvShow.close();
  }
}
