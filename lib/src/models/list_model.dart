class ListModel {
  int _page;
  int _totalResults;
  int _totalPages;
  List<_Result> _results = [];

  ListModel(this._page, this._totalResults, this._totalPages, this._results);

  factory ListModel.fromJson(Map<String, dynamic> data) {
    List<_Result> temp = [];
    for (int i = 0; i < data['results'].length; i++) {
      _Result result = _Result.fromJson((data['results'][i]));
      temp.add(result);
    }
    return ListModel(
        data["page"], data["total_results"], data["total_pages"], temp);
  }

  int get totalPages => _totalPages;
  int get totalResults => _totalResults;
  int get page => _page;
  List<_Result> get results => _results;
}

class _Result {
  int _id;
  String _posterPath;

  _Result(this._id, this._posterPath);

  factory _Result.fromJson(Map<String, dynamic> data) {
    return _Result(data["id"], data["poster_path"]);
  }

  int get id => _id;
  String get imageUrl => _posterPath;
}
