class ListDetailModel {
  int _id;
  String _title;
  String _overview;
  String _posterPath;
  bool _video;
  double _voteAverage;
  String _releaseDate;
  int _numberOfEpisodes;
  int _numberOfSeasons;

  ListDetailModel(this._id, this._title, this._overview, this._posterPath,
      this._video, this._voteAverage, this._releaseDate,
      [this._numberOfEpisodes, this._numberOfSeasons]);

  factory ListDetailModel.fromJson(Map<String, dynamic> data) {
    return ListDetailModel(
        data["id"],
        data["title"] ?? data["original_name"],
        data["overview"],
        data["poster_path"],
        data["video"],
        data["vote_average"],
        data["release_date"] ?? data["first_air_date"],
        data["number_of_episodes"],
        data["number_of_seasons"]);
  }

  int get id => _id;
  String get title => _title;
  String get overview => _overview;
  String get imageUrl => _posterPath;
  bool get video => _video;
  double get rating => _voteAverage;
  String get releaseDate => _releaseDate;
  int get noEpisodes => _numberOfEpisodes;
  int get noSeasons => _numberOfSeasons;
}
