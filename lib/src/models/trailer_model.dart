final _baseLink = "https://www.youtube.com/watch?v=";
final _baseThumbnail = "https://img.youtube.com/vi/";

class TrailerModel {
  int _id;
  List<_Result> _results = [];

  TrailerModel(this._id, this._results);

  factory TrailerModel.fromJson(Map<String, dynamic> data) {
    List<_Result> temp = [];
    for (int i = 0; i < data['results'].length; i++) {
      if (data['results'][i]["site"].toString().toLowerCase() == "youtube") {
        _Result result = _Result.fromJson((data['results'][i]));
        temp.add(result);
      }
    }

    return TrailerModel(data["id"], temp);
  }

  int get id => _id;
  List<_Result> get results => _results;
}

class _Result {
  String _id;
  String _name;
  String _site;
  String _videoUrl;
  String _thumbnail;

  _Result(this._id, this._name, this._site, this._videoUrl, this._thumbnail);

  factory _Result.fromJson(Map<String, dynamic> data) {
    return _Result(data["id"], data["name"], data["site"],
        '$_baseLink${data["key"]}', '$_baseThumbnail${data["key"]}/0.jpg');
  }

  String get id => _id;
  String get name => _name;
  String get site => _site;
  String get url => _videoUrl;
  String get thumbnail => _thumbnail;
}
