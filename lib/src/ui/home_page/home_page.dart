import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:gain_study_case/src/blocs/movie/movie_bloc.dart';
import 'package:gain_study_case/src/blocs/movie/movie_latest_bloc.dart';
import 'package:gain_study_case/src/blocs/movie/movie_list_bloc.dart';
import 'package:gain_study_case/src/blocs/tv_series/tv_show_bloc.dart';
import 'package:gain_study_case/src/blocs/tv_series/tv_show_latest_bloc.dart';
import 'package:gain_study_case/src/blocs/tv_series/tv_show_list_bloc.dart';
import 'package:gain_study_case/src/di/bloc_injector.dart';
import 'package:gain_study_case/src/models/list_detail_model.dart';
import 'package:gain_study_case/src/models/list_model.dart';
import 'package:gain_study_case/src/ui/custom_components/darken_image.dart';
import 'package:gain_study_case/src/ui/detail_page/detail_page.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rxdart/rxdart.dart';

class HomePage extends StatefulWidget {
  @override
  final MovieLatestBloc latestMovieBloc;
  final TvShowLatestBloc tvShowLatestBloc;

  final MovieListBloc movieListBloc;
  final TvShowListBloc tvShowListBloc;

  const HomePage(
      {Key key,
      this.latestMovieBloc,
      this.tvShowLatestBloc,
      this.movieListBloc,
      this.tvShowListBloc})
      : super(key: key);
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  int moviePage = 1, tvShowPage = 1;
  ListModel movieList;
  ListModel tvShowList;
  List<ListDetailModel> data = [];

  @override
  void initState() {
    super.initState();
    widget.latestMovieBloc.init();
    widget.latestMovieBloc.getLatestMovie();

    widget.tvShowLatestBloc.init();
    widget.tvShowLatestBloc.getLatestTvShow();

    widget.movieListBloc.init();
    widget.tvShowListBloc.init();
  }

  @override
  void dispose() {
    widget.latestMovieBloc.dispose();
    widget.tvShowLatestBloc.dispose();

    widget.movieListBloc.dispose();
    widget.tvShowListBloc.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
        top: false,
        bottom: false,
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Container(
                color: Colors.black,
                child: StreamBuilder(
                  stream: Rx.combineLatest2(
                    widget.latestMovieBloc.fetchLatestMovie,
                    widget.tvShowLatestBloc.fetchLatestTvShow,
                    (a, b) => {
                      if (a != null)
                        {this.data.add(ListDetailModel.fromJson(a))},
                      if (b != null)
                        {this.data.add(ListDetailModel.fromJson(b))},
                    },
                  ),
                  builder: (context, AsyncSnapshot<dynamic> snapshots) {
                    if (snapshots.hasData) {
                      widget.movieListBloc
                          .getMovieList({"page": moviePage.toString()});
                      widget.tvShowListBloc
                          .getTvShowList({"page": tvShowPage.toString()});
                      return _buildPage();
                    } else if (snapshots.hasError) {
                      return Text(snapshots.error.toString());
                    }
                    return Center(child: CircularProgressIndicator());
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildPage() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        CarouselSlider(
          items: [
            DarkenImage(
              isHomePage: true,
              isTVSeries: true,
              snapshot: this.data[1],
              onTap: () {
                _openDetailPage(1, true, this.data);
              },
            ),
            DarkenImage(
              isHomePage: true,
              isTVSeries: false,
              snapshot: data[0],
              onTap: () {
                _openDetailPage(0, false, this.data);
              },
            ),
          ],
          options: CarouselOptions(
            height: 400.0,
            viewportFraction: 1,
            enlargeCenterPage: true,
            autoPlayInterval: Duration(seconds: 15),
            autoPlayCurve: Curves.fastOutSlowIn,
            enableInfiniteScroll: true,
            autoPlay: true,
            autoPlayAnimationDuration: Duration(seconds: 1),
          ),
        ),
        _buildCarousel(true),
        _buildCarousel(false),
        //_buildCarousel(true),
      ],
    );
  }

  Widget _buildCarousel(bool isTvShow) {
    return Container(
      color: Colors.black12,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(20.0, 0, 20.0, 16.0),
            child: isTvShow == false
                ? Text(
                    "Popular Movies",
                    style: GoogleFonts.lato(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                  )
                : Text(
                    "Popular TV Shows",
                    style: GoogleFonts.lato(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.normal),
                  ),
          ),
          Container(
            width: double.infinity,
            height: 225.0,
            child: StreamBuilder(
              stream: isTvShow == false
                  ? widget.movieListBloc.fetchMovieList
                  : widget.tvShowListBloc.fetchTvShowList,
              builder: (context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData) {
                  isTvShow == false
                      ? this.moviePage == 1
                          ? this.movieList = ListModel.fromJson(snapshot.data)
                          : this.movieList.results.addAll(
                              (ListModel.fromJson(snapshot.data).results))
                      : this.tvShowPage == 1
                          ? this.tvShowList = ListModel.fromJson(snapshot.data)
                          : this.tvShowList.results.addAll(
                              (ListModel.fromJson(snapshot.data).results));
                  isTvShow == false ? this.moviePage++ : this.tvShowPage++;
                  return _buildList(
                    isTvShow == false ? this.movieList : this.tvShowList,
                    isTvShow,
                  );
                } else if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                }
                return Center(child: CircularProgressIndicator());
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildList(ListModel list, bool isTvShow) {
    ScrollController _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent ==
          _scrollController.position.pixels) {
        if (list.page < list.totalPages) {
          isTvShow == false
              ? widget.movieListBloc.getMovieList(
                  {"page": this.moviePage.toString()},
                )
              : widget.tvShowListBloc.getTvShowList(
                  {"page": this.tvShowPage.toString()},
                );
        }
      }
    });

    return ListView.builder(
      key: Key(list.page.toString()),
      controller: _scrollController,
      scrollDirection: Axis.horizontal,
      shrinkWrap: true,
      itemCount: list.results.length,
      itemBuilder: (context, index) {
        if (index == list.results.length) {
          return Center(child: CircularProgressIndicator());
        }
        return Padding(
          padding: EdgeInsets.fromLTRB(20.0, 0, 0, 16.0),
          child: GestureDetector(
            onTap: () => _openDetailPage(index, isTvShow, list.results),
            child: Card(
              elevation: 5,
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.grey[900], width: 1),
                borderRadius: BorderRadius.circular(20.0),
              ),
              color: Colors.white,
              child: Image.network(
                "https://image.tmdb.org/t/p/w500${list.results[index].imageUrl}",
                fit: BoxFit.fill,
                width: 150.0,
                height: 225.0,
              ),
            ),
          ),
        );
      },
    );
  }

  _openDetailPage(int index, bool isTVSeries, dynamic data) {
    Navigator.pushNamed(
      context,
      'detailPage',
      arguments: {
        "id": data[index].id.toString(),
        "isTvSeries": isTVSeries,
      },
    );
  }
}
