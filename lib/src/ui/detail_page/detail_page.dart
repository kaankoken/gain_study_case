import 'package:flutter/material.dart';
import 'package:gain_study_case/src/blocs/movie/movie_bloc.dart';
import 'package:gain_study_case/src/blocs/tv_series/tv_show_bloc.dart';
import 'package:gain_study_case/src/di/bloc_injector.dart';
import 'package:gain_study_case/src/models/list_detail_model.dart';
import 'package:gain_study_case/src/ui/custom_components/darken_image.dart';
import 'package:gain_study_case/src/ui/custom_components/text_view_holder.dart';

class DetailPage extends StatefulWidget {
  final isTVSeries;
  final id;

  final TvShowBloc tvShowBloc;
  final MovieBloc movieBloc;
  const DetailPage({
    Key key,
    this.isTVSeries,
    this.id,
    this.tvShowBloc,
    this.movieBloc,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return DetailPageState(this.isTVSeries, this.id);
  }
}

class DetailPageState extends State<DetailPage> {
  final isTVSeries;
  String id;

  DetailPageState(this.isTVSeries, this.id);

  @override
  void initState() {
    super.initState();
    if (this.isTVSeries) {
      widget.tvShowBloc.init();
      widget.tvShowBloc.getTvShowDetail({"tv_id": this.id.toString()});
    } else {
      widget.movieBloc.init();
      widget.movieBloc.getMovieDetail({"movie_id": this.id.toString()});
    }
  }

  @override
  void dispose() {
    if (this.isTVSeries) {
      widget.tvShowBloc.dispose();
    } else {
      widget.movieBloc.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
        top: false,
        bottom: false,
        child: StreamBuilder(
          stream: isTVSeries == false
              ? widget.movieBloc.fetchMovieDetail
              : widget.tvShowBloc.fetchTvShowDetail,
          builder: (context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              return _buildPage(snapshot);
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            }
            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }

  Widget _buildPage(AsyncSnapshot<dynamic> snapshot) {
    final data = ListDetailModel.fromJson(snapshot.data);
    return CustomScrollView(
      slivers: [
        SliverAppBar(
          leading: Column(),
          actions: [
            Container(
              margin: EdgeInsets.only(right: 20.0),
              child: InkWell(
                child: Icon(
                  Icons.close_outlined,
                  color: Colors.white,
                  size: 30,
                ),
                onTap: () => Navigator.of(context).pop(),
              ),
              decoration: BoxDecoration(
                color: Colors.black,
                shape: BoxShape.circle,
              ),
            ),
          ],
          backgroundColor: Colors.black,
          expandedHeight: 380.0,
          pinned: false,
          floating: false,
          elevation: 0.0,
          stretch: true,
          flexibleSpace: FlexibleSpaceBar(
            background: DarkenImage(
              isHomePage: false,
              isTVSeries: this.isTVSeries,
              snapshot: data,
              onTap: () {},
            ),
          ),
        ),
        SliverToBoxAdapter(
            child: Container(
          color: Colors.black,
          child: Padding(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextViewHolder(isTVShow: this.isTVSeries, data: data)
              ],
            ),
          ),
        ))
      ],
    );
  }
}
