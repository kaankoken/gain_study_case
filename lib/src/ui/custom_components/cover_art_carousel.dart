import 'package:flutter/material.dart';

class CoverArtCarousel extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CoverArtCarouselState();
  }
}

class CoverArtCarouselState extends State<CoverArtCarousel> {
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: double.infinity,
            height: 225.0,
            child: ListView(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 0, 0, 16.0),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: Colors.white, width: 1),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    color: Colors.white,
                    child: Image.network(
                      "https://image.tmdb.org/t/p/w500/pB8BM7pdSp6B6Ih7QZ4DrQ3PmJK.jpg",
                      fit: BoxFit.cover,
                      width: 150.0,
                      height: 225.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
