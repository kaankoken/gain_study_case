import 'dart:io';

import 'package:flutter/material.dart';
import 'package:gain_study_case/src/blocs/movie/movie_trailer_bloc.dart';
import 'package:gain_study_case/src/blocs/tv_series/tv_show_trailer_bloc.dart';
import 'package:gain_study_case/src/models/trailer_model.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:uuid/uuid.dart';

class ImageButtonHolder extends StatelessWidget {
  static final greenColor = Color.fromRGBO(77, 247, 135, 1);
  static final lightGreen = Color.fromRGBO(183, 221, 41, 1);
  static final yellowColor = Color.fromRGBO(225, 226, 52, 1);
  static final orangeColor = Color.fromRGBO(255, 165, 52, 1);
  static final redColor = Color.fromRGBO(216, 54, 54, 1);
  static final textGray = Color.fromRGBO(165, 165, 165, 1);

  final isHomePage;
  final isTVSeries;
  final data;

  final Uuid uuid = Uuid();
  final MovieTrailerBloc movieTrailerBloc;
  final TvShowTrailerBloc tvShowTrailerBloc;

  ImageButtonHolder({
    Key key,
    this.isHomePage,
    this.isTVSeries,
    this.data,
    this.movieTrailerBloc,
    this.tvShowTrailerBloc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (this.isTVSeries == false) {
      this.movieTrailerBloc.init();
      this
          .movieTrailerBloc
          .getMovieTrailer({"movie_id": this.data.id.toString()});
    } else {
      this.tvShowTrailerBloc.init();
      this
          .tvShowTrailerBloc
          .getTvSeriesTrailer({"tv_id": this.data.id.toString()});
    }
    return Container(
      child: IntrinsicHeight(
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: isHomePage == false
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: [
                            Container(
                              width: 35,
                              height: 35,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 2.5,
                                  color: _chooseColor(this.data.rating),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(35),
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  this.data.rating.toString(),
                                  style: GoogleFonts.lato(
                                    fontSize: 12,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(10.0, 0, 25.0, 0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${this.data.title ?? "-"}',
                                      style: GoogleFonts.lato(
                                        fontSize: 18,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                    ),
                                    this.isTVSeries == false
                                        ? Text(
                                            "Movie",
                                            style: GoogleFonts.lato(
                                              fontSize: 14,
                                              color: textGray,
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          )
                                        : Text(
                                            "TV Show",
                                            style: GoogleFonts.lato(
                                              fontSize: 14,
                                              color: textGray,
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ],
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          this.data.title,
                          style: GoogleFonts.lato(fontSize: 18),
                        ),
                        this.isTVSeries == false
                            ? Text(
                                "Movie",
                                style: GoogleFonts.lato(
                                  fontSize: 14,
                                  color: textGray,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            : Text(
                                "TV Show",
                                style: GoogleFonts.lato(
                                  fontSize: 14,
                                  color: textGray,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                        Text(
                          "Latest content",
                          style: GoogleFonts.lato(
                            fontSize: 12,
                            color: redColor,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
            ),
            Expanded(
              flex: 0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  ElevatedButton(
                    onPressed: () => showModalBottomSheet(
                      context: context,
                      isScrollControlled: true,
                      backgroundColor: Colors.transparent,
                      builder: (_) {
                        return DraggableScrollableSheet(
                          expand: false,
                          initialChildSize: 0.8,
                          minChildSize: 0.79,
                          maxChildSize: 0.8,
                          builder: (BuildContext context,
                              ScrollController scrollController) {
                            return Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(16.0),
                              ),
                              child: StreamBuilder(
                                stream: this.isTVSeries == false
                                    ? this.movieTrailerBloc.fetchMovieTrailer
                                    : this.tvShowTrailerBloc.fetchTvShowTrailer,
                                builder:
                                    (context, AsyncSnapshot<dynamic> snapshot) {
                                  if (snapshot.hasData) {
                                    return Padding(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 16.0),
                                      child: _bottomSheetBuilder(
                                        TrailerModel.fromJson(snapshot.data),
                                        scrollController,
                                        context,
                                      ),
                                    );
                                  } else if (snapshot.hasError) {
                                    return Text(snapshot.error.toString());
                                  }
                                  return Center(
                                      child: CircularProgressIndicator());
                                },
                              ),
                            );
                          },
                        );
                      },
                    ),
                    child: Text(
                      "Watch",
                      style: GoogleFonts.lato(
                        fontSize: 12,
                        color: Colors.black,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      shape: StadiumBorder(),
                      primary: greenColor,
                      minimumSize: Size(93.0, 35.0),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Color _chooseColor(double rating) {
    if (rating <= 10.0 && rating > 8.0) {
      return greenColor;
    } else if (rating <= 8.0 && rating > 6.0) {
      return lightGreen;
    } else if (rating <= 6.0 && rating > 4.0) {
      return yellowColor;
    } else if (rating <= 4.0 && rating > 2.0) {
      return orangeColor;
    } else {
      return redColor;
    }
  }

  Widget _bottomSheetBuilder(
      TrailerModel trailerModel, ScrollController scrollController, context) {
    var listStatus = trailerModel.results.length == 0;

    return GridView.builder(
      key: Key(this.uuid.v1().toString()),
      shrinkWrap: true,
      controller: scrollController,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount:
            MediaQuery.of(context).orientation == Orientation.landscape ? 2 : 1,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        childAspectRatio: (2 / 1),
      ),
      itemCount:
          trailerModel.results.length == 0 ? 1 : trailerModel.results.length,
      itemBuilder: (context, index) {
        if (listStatus) {
          return Card(
              elevation: 5,
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: Container(
                child: Center(
                  child: Text(
                    "No eligable video found",
                    style: GoogleFonts.lato(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ),
              ));
        } else {
          return GestureDetector(
            onTap: () => _launchUrl(trailerModel.results[index].url),
            child: Card(
              semanticContainer: true,
              color: Colors.white,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: Stack(
                children: <Widget>[
                  Image.network(
                    trailerModel.results[index].thumbnail,
                    fit: BoxFit.cover,
                    height: MediaQuery.of(context).size.width,
                    width: double.infinity,
                  ),
                  Container(
                    margin: EdgeInsets.all(5.0),
                    height: MediaQuery.of(context).size.width,
                    width: double.infinity,
                    child: Center(child: Icon(Icons.play_circle_filled)),
                  ),
                ],
              ),
            ),
          );
        }
      },
    );
  }

  void _launchUrl(String link) async {
    if (Platform.isIOS) {
      if (await canLaunch('youtube://$link')) {
        await launch('youtube://$link', forceSafariVC: false);
      } else {
        if (await canLaunch('$link')) {
          await launch('$link');
        } else {
          throw 'Could not launch $link';
        }
      }
    } else if (Platform.isAndroid) {
      if (await canLaunch(link)) {
        await launch(link);
      } else {
        throw 'Could not launch $link';
      }
    }
  }
}
