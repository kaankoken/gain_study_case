import 'package:flutter/material.dart';
import 'package:gain_study_case/src/blocs/movie/movie_trailer_bloc.dart';
import 'package:gain_study_case/src/blocs/tv_series/tv_show_trailer_bloc.dart';
import 'package:gain_study_case/src/di/bloc_injector.dart';
import 'package:gain_study_case/src/ui/custom_components/image_button_holder.dart';

class DarkenImage extends StatelessWidget {
  final isHomePage;
  final isTVSeries;
  final snapshot;

  final VoidCallback onTap;

  const DarkenImage(
      {Key key, this.isHomePage, this.isTVSeries, this.snapshot, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        this.onTap();
      },
      child: Container(
        child: Stack(
          children: [
            this.snapshot.imageUrl != null && this.snapshot.imageUrl != ""
                ? Image.network(
                    "https://image.tmdb.org/t/p/w500${this.snapshot.imageUrl}",
                    fit: BoxFit.cover,
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height,
                  )
                : Image.asset("assets/noImageFound.png"),
            ShadeWidget(),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height,
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: EdgeInsets.fromLTRB(20.0, 0, 20.0, 40.0),
                child: ImageButtonHolder(
                  isHomePage: isHomePage,
                  isTVSeries: isTVSeries,
                  data: this.snapshot,
                  movieTrailerBloc: getIt<MovieTrailerBloc>(),
                  tvShowTrailerBloc: getIt<TvShowTrailerBloc>(),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ShadeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            const Color(0x00000000),
            const Color(0x00000000),
            const Color(0x44000000),
            const Color(0x44000000),
            const Color(0xBB000000),
            const Color(0xDD000000),
            const Color(0xFF000000),
          ],
        ),
      ),
    );
  }
}
