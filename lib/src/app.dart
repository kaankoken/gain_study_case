import 'package:flutter/material.dart';
import 'package:gain_study_case/src/blocs/movie/movie_bloc.dart';
import 'package:gain_study_case/src/blocs/movie/movie_latest_bloc.dart';
import 'package:gain_study_case/src/blocs/movie/movie_list_bloc.dart';
import 'package:gain_study_case/src/blocs/tv_series/tv_show_bloc.dart';
import 'package:gain_study_case/src/blocs/tv_series/tv_show_latest_bloc.dart';
import 'package:gain_study_case/src/blocs/tv_series/tv_show_list_bloc.dart';
import 'package:gain_study_case/src/di/bloc_injector.dart';
import 'package:gain_study_case/src/ui/detail_page/detail_page.dart';
import 'package:gain_study_case/src/ui/home_page/home_page.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      initialRoute: '/',
      onGenerateRoute: (settings) {
        if (settings.name == "detailPage") {
          final Map<String, dynamic> data = settings.arguments;
          return MaterialPageRoute(
            builder: (context) {
              return DetailPage(
                isTVSeries: data["isTvSeries"],
                id: data["id"],
                tvShowBloc: getIt<TvShowBloc>(),
                movieBloc: getIt<MovieBloc>(),
              );
            },
          );
        }
      },
      routes: {
        '/': (context) => HomePage(
              movieListBloc: getIt<MovieListBloc>(),
              latestMovieBloc: getIt<MovieLatestBloc>(),
              tvShowListBloc: getIt<TvShowListBloc>(),
              tvShowLatestBloc: getIt<TvShowLatestBloc>(),
            ),
      },
    );
  }
}

// DetailPage(isTVSeries: true, id: "63174")