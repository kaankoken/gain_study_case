import 'package:gain_study_case/src/blocs/movie/movie_bloc.dart';
import 'package:gain_study_case/src/blocs/movie/movie_latest_bloc.dart';
import 'package:gain_study_case/src/blocs/movie/movie_list_bloc.dart';
import 'package:gain_study_case/src/blocs/movie/movie_trailer_bloc.dart';
import 'package:gain_study_case/src/blocs/tv_series/tv_show_bloc.dart';
import 'package:gain_study_case/src/blocs/tv_series/tv_show_latest_bloc.dart';
import 'package:gain_study_case/src/blocs/tv_series/tv_show_list_bloc.dart';
import 'package:gain_study_case/src/blocs/tv_series/tv_show_trailer_bloc.dart';
import 'package:gain_study_case/src/resources/network_helper.dart';
import 'package:gain_study_case/src/resources/repositories/movie_repository.dart';
import 'package:gain_study_case/src/resources/repositories/tv_show_repository.dart';
import 'package:gain_study_case/src/ui/detail_page/detail_page.dart';
import 'package:gain_study_case/src/ui/home_page/home_page.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:injectable/injectable.dart';
import 'bloc_injector.config.dart';

final GetIt getIt = GetIt.instance;
final Client _client = Client();

@injectableInit
Future<void> configureInjection() async {
  //$initGetIt(getIt);

  getIt.registerSingleton<NetworkHelper>(NetworkHelper(_client));

  getIt.registerSingleton<MovieRepository>(
      MovieRepository(getIt.get<NetworkHelper>()));
  getIt.registerLazySingleton<MovieBloc>(
      () => MovieBloc(getIt.get<MovieRepository>()));
  getIt.registerLazySingleton<MovieListBloc>(
      () => MovieListBloc(getIt.get<MovieRepository>()));
  getIt.registerLazySingleton<MovieLatestBloc>(
      () => MovieLatestBloc(getIt.get<MovieRepository>()));
  getIt.registerLazySingleton<MovieTrailerBloc>(
      () => MovieTrailerBloc(getIt.get<MovieRepository>()));

  getIt.registerSingleton<TvShowRepository>(
      TvShowRepository(getIt.get<NetworkHelper>()));
  getIt.registerLazySingleton<TvShowBloc>(
      () => TvShowBloc(getIt.get<TvShowRepository>()));
  getIt.registerLazySingleton<TvShowListBloc>(
      () => TvShowListBloc(getIt.get<TvShowRepository>()));
  getIt.registerLazySingleton<TvShowLatestBloc>(
      () => TvShowLatestBloc(getIt.get<TvShowRepository>()));
  getIt.registerLazySingleton<TvShowTrailerBloc>(
      () => TvShowTrailerBloc(getIt.get<TvShowRepository>()));
}
