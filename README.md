# GAİN Flutter Study Case
Created by Kaan Taha Köken

## Content
- BloC Pattern
- Dependency Injection
- Network Layer
- Reusable Components
- Unit Test
- Separate Repositories for scalability
- 3rd Party Library usages
    - Carousel Slider
    - RxDart
    - http
    - DotEnv
    - Injectable
    - GetIt
    - Provider
    - Url Launcher
    - Build runner
    - Mockito
    - Injectable Generator
- Environment usage with DotEnv

This project can run directly iOS emulator or any Android devices (emulators and physical devices).
If you wish to recompile the project, follow these steps
## Steps:
- flutter clean
- flutter pub get
- flutter packages pub run build_runner build --delete-conflicting-outputs
    - After this step two files will be generated which are provider_injector.config.dart under test/unit_test and
    bloc_injector.config.dart under lib/src/di.<br>Please remove question marks (?) on both files. Vscode will show errors. (<b><i>Flutter version and build runner version causing compatibility issue<br>with other packages. Downgrading did not solve the issue and increased the issues.</i></b>)
- After these steps, it is ready to launch.

