import 'dart:io';

import 'package:flutter_test/flutter_test.dart' as unit_test;
import 'package:flutter_test/flutter_test.dart';
import 'package:gain_study_case/src/models/state.dart';
import 'package:gain_study_case/src/resources/apis.dart';
import 'package:gain_study_case/src/resources/network_helper.dart';
import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;
import 'package:mockito/mockito.dart';

import 'mock_client.dart';
import 'provider_injector.dart';

Future main() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  await DotEnv.dotenv.load(fileName: ".env");

  final _apiKey = DotEnv.dotenv.env['MOVIEDB_API_KEY'];
  await configureMockInjection();

  ProviderTest app = getIt<ProviderTest>();
  MockClient mockClient = app.mockClient;
  NetworkHelper helper = app.networkHelper;

  String _replacePathParameters(
      String uri, Map<String, String> pathParameters) {
    String editedUri = uri.toString();
    pathParameters.forEach((key, value) {
      editedUri = editedUri.replaceFirst("{$key}", value);
    });

    return editedUri;
  }

  /**
   * Popular Movies Success and Fail Case
   */
  unit_test.group(
    "Network Testing",
    () {
      unit_test.test(
        "Popular movies success",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;

          when(mockClient.get(Uri.parse(APIs.POPULAR_MOVIES)
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"page": 1,"total_results": 19832,"total_pages": 992,"results": [{"vote_count": 302,"id": 420818,"video": false,"vote_average": 6.8,"title": "The Lion King","popularity": 502.676,"poster_path": "/dzBtMocZuJbjLOXvrl4zGYigDzh.jpg","original_language": "en","original_title": "The Lion King","genre_ids": [12,16,10751,18,28],"backdrop_path": "/1TUg5pO1VZ4B0Q1amk3OlXvlpXV.jpg","adult": false,"overview": "Simba idolises his father, King Mufasa, and takes to heart his own royal destiny. But not everyone in the kingdom celebrates the new cub\'s arrival. Scar, Mufasa\'s brother—and former heir to the throne—has plans of his own. The battle for Pride Rock is ravaged with betrayal, tragedy and drama, ultimately resulting in Simba\'s exile. With help from a curious pair of newfound friends, Simba will have to figure out how to grow up and take back what is rightfully his.","release_date": "2019-07-12"}]}',
              200,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(RequestMethod.GET, APIs.POPULAR_MOVIES),
              unit_test.isInstanceOf<SuccessState>());
        },
      );

      unit_test.test(
        "Popular movies fail",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;

          when(mockClient.get(Uri.parse(APIs.POPULAR_MOVIES)
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"status_code":7,"status_message":"Invalid API key: You must be granted a valid key.","success":false}',
              401,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(RequestMethod.GET, APIs.POPULAR_MOVIES),
              unit_test.isInstanceOf<ErrorState>());
        },
      );

      /**
       * Popular Tv Series Success and Fail Case
       */

      unit_test.test(
        "Popular tv series success",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;

          when(mockClient.get(Uri.parse(APIs.POPULAR_TVSHOWS)
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"page": 1,"total_results": 10000,"total_pages": 500,"results": [{"backdrop_path":"/h48Dpb7ljv8WQvVdyFWVLz64h4G.jpg","first_air_date":"2016-01-25","genre_ids":[80,10765],"id":63174,"name":"Lucifer","origin_country":["US"],"original_language":"en","original_name":"Lucifer","overview":"Bored and unhappy as the Lord of Hell, Lucifer Morningstar abandoned his throne and retired to Los Angeles, where he has teamed up with LAPD detective Chloe Decker to take down criminals. But the longer he\'s away from the underworld, the greater the threat that the worst of humanity could escape.\","popularity":1391.815,"poster_path":"/4EYPN5mVIhKLfxGruy7Dy41dTVn.jpg","vote_average":8.5,"vote_count":9175}]}',
              200,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(RequestMethod.GET, APIs.POPULAR_TVSHOWS),
              unit_test.isInstanceOf<SuccessState>());
        },
      );

      unit_test.test(
        "Popular tv series fail",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;

          when(mockClient.get(Uri.parse(APIs.POPULAR_TVSHOWS)
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"status_code":7,"status_message":"Invalid API key: You must be granted a valid key.","success":false}',
              401,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(RequestMethod.GET, APIs.POPULAR_TVSHOWS),
              unit_test.isInstanceOf<ErrorState>());
        },
      );

      /**
       * Movie Detail success and fail case
       * id => 337404
       */

      unit_test.test(
        "Movie detail success",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;
          when(mockClient.get(Uri.parse(_replacePathParameters(
                      APIs.MOVIE_DETAIL, {"movie_id": "337404"}))
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"adult":false,"backdrop_path":"/6MKr3KgOLmzOP6MSuZERO41Lpkt.jpg","belongs_to_collection":{"id":837007,"name":"Cruella Collection","poster_path":null,"backdrop_path":null},"budget":200000000,"genres":[{"id":35,"name":"Comedy"},{"id":80,"name":"Crime"}],"homepage":"https://movies.disney.com/cruella","id":337404,"imdb_id":"tt3228774","original_language":"en","original_title":"Cruella","overview":"In 1970s London amidst the punk rock revolution, a young grifter named Estella is determined to make a name for herself with her designs. She befriends a pair of young thieves who appreciate her appetite for mischief, and together they are able to build a life for themselves on the London streets. One day, Estella’s flair for fashion catches the eye of the Baroness von Hellman, a fashion legend who is devastatingly chic and terrifyingly haute. But their relationship sets in motion a course of events and revelations that will cause Estella to embrace her wicked side and become the raucous, fashionable and revenge-bent Cruella.","popularity":5507.395,"poster_path":"/rTh4K5uw9HypmpGslcKd4QfHl93.jpg","production_companies":[{"id":2,"logo_path":"/wdrCwmRnLFJhEoH8GSfymY85KHT.png","name":"Walt Disney Pictures","origin_country":"US"}],"production_countries":[{"iso_3166_1":"US","name":"United States of America"}],"release_date":"2021-05-26","revenue":88197497,"runtime":134,"spoken_languages":[{"english_name":"English","iso_639_1":"en","name":"English"}],"status":"Released","tagline":"Hello Cruel World","title":"Cruella","video":false,"vote_average":8.7,"vote_count":2418}',
              200,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(
                RequestMethod.GET,
                APIs.MOVIE_DETAIL,
                pathParameters: {"movie_id": "337404"},
              ),
              unit_test.isInstanceOf<SuccessState>());
        },
      );

      unit_test.test(
        "Movie detail fail",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;

          when(mockClient.get(Uri.parse(_replacePathParameters(
                      APIs.MOVIE_DETAIL, {"movie_id": "337404"}))
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"status_code":7,"status_message":"Invalid API key: You must be granted a valid key.","success":false}',
              401,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(
                RequestMethod.GET,
                APIs.MOVIE_DETAIL,
                pathParameters: {"movie_id": "337404"},
              ),
              unit_test.isInstanceOf<ErrorState>());
        },
      );

      /**
       * Tv series detail success and fail case
       * id => 84958
       */

      unit_test.test(
        "Tv series detail success",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;
          when(mockClient.get(Uri.parse(_replacePathParameters(
                      APIs.TVSHOW_DETAIL, {"tv_id": "84958"}))
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"adult":false,"backdrop_path":"/6MKr3KgOLmzOP6MSuZERO41Lpkt.jpg","belongs_to_collection":{"id":837007,"name":"Cruella Collection","poster_path":null,"backdrop_path":null},"budget":200000000,"genres":[{"id":35,"name":"Comedy"},{"id":80,"name":"Crime"}],"homepage":"https://movies.disney.com/cruella","id":337404,"imdb_id":"tt3228774","original_language":"en","original_title":"Cruella","overview":"In 1970s London amidst the punk rock revolution, a young grifter named Estella is determined to make a name for herself with her designs. She befriends a pair of young thieves who appreciate her appetite for mischief, and together they are able to build a life for themselves on the London streets. One day, Estella’s flair for fashion catches the eye of the Baroness von Hellman, a fashion legend who is devastatingly chic and terrifyingly haute. But their relationship sets in motion a course of events and revelations that will cause Estella to embrace her wicked side and become the raucous, fashionable and revenge-bent Cruella.","popularity":5507.395,"poster_path":"/rTh4K5uw9HypmpGslcKd4QfHl93.jpg","production_companies":[{"id":2,"logo_path":"/wdrCwmRnLFJhEoH8GSfymY85KHT.png","name":"Walt Disney Pictures","origin_country":"US"}],"production_countries":[{"iso_3166_1":"US","name":"United States of America"}],"release_date":"2021-05-26","revenue":88197497,"runtime":134,"spoken_languages":[{"english_name":"English","iso_639_1":"en","name":"English"}],"status":"Released","tagline":"Hello Cruel World","title":"Cruella","video":false,"vote_average":8.7,"vote_count":2418}',
              200,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(
                RequestMethod.GET,
                APIs.TVSHOW_DETAIL,
                pathParameters: {"tv_id": "84958"},
              ),
              unit_test.isInstanceOf<SuccessState>());
        },
      );

      unit_test.test(
        "Tv Series detail fail",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;

          when(mockClient.get(Uri.parse(_replacePathParameters(
                      APIs.TVSHOW_DETAIL, {"tv_id": "84958"}))
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"status_code":7,"status_message":"Invalid API key: You must be granted a valid key.","success":false}',
              401,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(
                RequestMethod.GET,
                APIs.TVSHOW_DETAIL,
                pathParameters: {"tv_id": "84958"},
              ),
              unit_test.isInstanceOf<ErrorState>());
        },
      );

      /**
       * Latest movie success and fail case
       */

      unit_test.test(
        "Latest movie success",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;
          when(mockClient.get(Uri.parse(APIs.LATEST_MOVIE)
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"adult":false,"backdrop_path":null,"belongs_to_collection":null,"budget":0,"genres":[],"homepage":"https://youtu.be/ICLluY0Y1bg","id":839444,"imdb_id":"tt14727576","original_language":"en","original_title":"Bathroom 3: The Other Side","overview":"When Sky disappears and a police investigation is started up, his friends Jackson and Allen try to figure out what happened to him, but might meet the same fate.","popularity":0.0,"poster_path":null,"production_companies":[],"production_countries":[],"release_date":"","revenue":0,"runtime":2,"spoken_languages":[],"status":"Released","tagline":"","title":"Bathroom 3: The Other Side","video":false,"vote_average":0.0,"vote_count":0}',
              200,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(
                RequestMethod.GET,
                APIs.LATEST_MOVIE,
              ),
              unit_test.isInstanceOf<SuccessState>());
        },
      );

      unit_test.test(
        "Latest movie fail",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;

          when(mockClient.get(Uri.parse(APIs.LATEST_MOVIE)
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"status_code":7,"status_message":"Invalid API key: You must be granted a valid key.","success":false}',
              401,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(
                RequestMethod.GET,
                APIs.LATEST_MOVIE,
              ),
              unit_test.isInstanceOf<ErrorState>());
        },
      );

      /**
       * Latest tv series success and fail case
       */

      unit_test.test(
        "Latest tv series success",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;
          when(mockClient.get(Uri.parse(APIs.LATEST_TVSHOW)
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"backdrop_path":null,"created_by":[],"episode_run_time":[],"first_air_date":"2021-06-11","genres":[],"homepage":"","id":127389,"in_production":true,"languages":[],"last_air_date":"2021-06-11","last_episode_to_air":{"air_date":"2021-06-11","episode_number":1,"id":3019741,"name":"","overview":"","production_code":"","season_number":1,"still_path":null,"vote_average":0.0,"vote_count":0},"name":"Latin Flow","next_episode_to_air":null,"networks":[],"number_of_episodes":1,"number_of_seasons":1,"origin_country":[],"original_language":"pt","original_name":"Latin Flow","overview":"","popularity":1.4,"poster_path":"/a6uaDne4gH8Kp0Svk8iCuf6p5gQ.jpg","production_companies":[],"production_countries":[],"seasons":[{"air_date":"2021-06-11","episode_count":1,"id":198928,"name":"Season 1","overview":"","poster_path":null,"season_number":1}],"spoken_languages":[],"status":"Returning Series","tagline":"","type":"Scripted","vote_average":0.0,"vote_count":0}',
              200,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(
                RequestMethod.GET,
                APIs.LATEST_TVSHOW,
              ),
              unit_test.isInstanceOf<SuccessState>());
        },
      );

      unit_test.test(
        "Latest tv series fail",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;

          when(mockClient.get(Uri.parse(APIs.LATEST_TVSHOW)
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"status_code":7,"status_message":"Invalid API key: You must be granted a valid key.","success":false}',
              401,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(
                RequestMethod.GET,
                APIs.LATEST_TVSHOW,
              ),
              unit_test.isInstanceOf<ErrorState>());
        },
      );

      /**
       * Movie trailer success and fail case
       * id => 337404
       */
      unit_test.test(
        "Movie trailer success",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;
          when(mockClient.get(Uri.parse(_replacePathParameters(
                      APIs.WATCH_MOVIE_TRAILER, {"movie_id": "337404"}))
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"backdrop_path":null,"created_by":[],"episode_run_time":[],"first_air_date":"2021-06-11","genres":[],"homepage":"","id":127389,"in_production":true,"languages":[],"last_air_date":"2021-06-11","last_episode_to_air":{"air_date":"2021-06-11","episode_number":1,"id":3019741,"name":"","overview":"","production_code":"","season_number":1,"still_path":null,"vote_average":0.0,"vote_count":0},"name":"Latin Flow","next_episode_to_air":null,"networks":[],"number_of_episodes":1,"number_of_seasons":1,"origin_country":[],"original_language":"pt","original_name":"Latin Flow","overview":"","popularity":1.4,"poster_path":"/a6uaDne4gH8Kp0Svk8iCuf6p5gQ.jpg","production_companies":[],"production_countries":[],"seasons":[{"air_date":"2021-06-11","episode_count":1,"id":198928,"name":"Season 1","overview":"","poster_path":null,"season_number":1}],"spoken_languages":[],"status":"Returning Series","tagline":"","type":"Scripted","vote_average":0.0,"vote_count":0}',
              200,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(
                RequestMethod.GET,
                APIs.WATCH_MOVIE_TRAILER,
                pathParameters: {"movie_id": "337404"},
              ),
              unit_test.isInstanceOf<SuccessState>());
        },
      );

      unit_test.test(
        "Movie trailer fail",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;

          when(mockClient.get(Uri.parse(_replacePathParameters(
                      APIs.WATCH_MOVIE_TRAILER, {"movie_id": "337404"}))
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"status_code":7,"status_message":"Invalid API key: You must be granted a valid key.","success":false}',
              401,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(
                RequestMethod.GET,
                APIs.WATCH_MOVIE_TRAILER,
                pathParameters: {"movie_id": "337404"},
              ),
              unit_test.isInstanceOf<ErrorState>());
        },
      );

      /**
       * Tv Series trailer success and fail case
       * id => 337404
       */
      unit_test.test(
        "Tv Series trailer success",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;
          when(mockClient.get(Uri.parse(_replacePathParameters(
                      APIs.WATCH_TV_SERIES_TRAILER, {"tv_id": "84958"}))
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"backdrop_path":null,"created_by":[],"episode_run_time":[],"first_air_date":"2021-06-11","genres":[],"homepage":"","id":127389,"in_production":true,"languages":[],"last_air_date":"2021-06-11","last_episode_to_air":{"air_date":"2021-06-11","episode_number":1,"id":3019741,"name":"","overview":"","production_code":"","season_number":1,"still_path":null,"vote_average":0.0,"vote_count":0},"name":"Latin Flow","next_episode_to_air":null,"networks":[],"number_of_episodes":1,"number_of_seasons":1,"origin_country":[],"original_language":"pt","original_name":"Latin Flow","overview":"","popularity":1.4,"poster_path":"/a6uaDne4gH8Kp0Svk8iCuf6p5gQ.jpg","production_companies":[],"production_countries":[],"seasons":[{"air_date":"2021-06-11","episode_count":1,"id":198928,"name":"Season 1","overview":"","poster_path":null,"season_number":1}],"spoken_languages":[],"status":"Returning Series","tagline":"","type":"Scripted","vote_average":0.0,"vote_count":0}',
              200,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(
                RequestMethod.GET,
                APIs.WATCH_TV_SERIES_TRAILER,
                pathParameters: {"tv_id": "84958"},
              ),
              unit_test.isInstanceOf<SuccessState>());
        },
      );

      unit_test.test(
        "Tv Series trailer fail",
        () async {
          var queryParameters = Map<String, String>();
          queryParameters["api_key"] = _apiKey;

          when(mockClient.get(Uri.parse(_replacePathParameters(
                      APIs.WATCH_TV_SERIES_TRAILER, {"tv_id": "84958"}))
                  .replace(queryParameters: queryParameters)))
              .thenAnswer(
            (_) async => http.Response(
              '{"status_code":7,"status_message":"Invalid API key: You must be granted a valid key.","success":false}',
              401,
              headers: {
                HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
              },
            ),
          );

          unit_test.expect(
              await helper.sendRequest(
                RequestMethod.GET,
                APIs.WATCH_TV_SERIES_TRAILER,
                pathParameters: {"tv_id": "84958"},
              ),
              unit_test.isInstanceOf<ErrorState>());
        },
      );
    },
  );
}

@injectable
class ProviderTest {
  final NetworkHelper networkHelper;
  final http.Client mockClient;

  ProviderTest(this.networkHelper, this.mockClient) : super();
}
