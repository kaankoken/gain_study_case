// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:gain_study_case/src/blocs/movie/movie_bloc.dart' as _i12;
import 'package:gain_study_case/src/blocs/movie/movie_latest_bloc.dart' as _i13;
import 'package:gain_study_case/src/blocs/movie/movie_list_bloc.dart' as _i14;
import 'package:gain_study_case/src/blocs/movie/movie_trailer_bloc.dart' as _i8;
import 'package:gain_study_case/src/blocs/tv_series/tv_show_bloc.dart' as _i9;
import 'package:gain_study_case/src/blocs/tv_series/tv_show_latest_bloc.dart'
    as _i10;
import 'package:gain_study_case/src/blocs/tv_series/tv_show_list_bloc.dart'
    as _i11;
import 'package:gain_study_case/src/blocs/tv_series/tv_show_trailer_bloc.dart'
    as _i6;
import 'package:gain_study_case/src/resources/network_helper.dart' as _i3;
import 'package:gain_study_case/src/resources/repositories/movie_repository.dart'
    as _i7;
import 'package:gain_study_case/src/resources/repositories/tv_show_repository.dart'
    as _i5;
import 'package:get_it/get_it.dart' as _i1;
import 'package:http/http.dart' as _i4;
import 'package:injectable/injectable.dart'
    as _i2; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String environment, _i2.EnvironmentFilter environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.factory<_i3.NetworkHelper>(() => _i3.NetworkHelper(get<_i4.Client>()));
  gh.factory<_i5.TvShowRepository>(
      () => _i5.TvShowRepository(get<_i3.NetworkHelper>()));
  gh.factory<_i6.TvShowTrailerBloc>(
      () => _i6.TvShowTrailerBloc(get<_i5.TvShowRepository>()));
  gh.factory<_i7.MovieRepository>(
      () => _i7.MovieRepository(get<_i3.NetworkHelper>()));
  gh.factory<_i8.MovieTrailerBloc>(
      () => _i8.MovieTrailerBloc(get<_i7.MovieRepository>()));
  gh.factory<_i9.TvShowBloc>(() => _i9.TvShowBloc(get<_i5.TvShowRepository>()));
  gh.factory<_i10.TvShowLatestBloc>(
      () => _i10.TvShowLatestBloc(get<_i5.TvShowRepository>()));
  gh.factory<_i11.TvShowListBloc>(
      () => _i11.TvShowListBloc(get<_i5.TvShowRepository>()));
  gh.factory<_i12.MovieBloc>(() => _i12.MovieBloc(get<_i7.MovieRepository>()));
  gh.factory<_i13.MovieLatestBloc>(
      () => _i13.MovieLatestBloc(get<_i7.MovieRepository>()));
  gh.factory<_i14.MovieListBloc>(
      () => _i14.MovieListBloc(get<_i7.MovieRepository>()));
  return get;
}
