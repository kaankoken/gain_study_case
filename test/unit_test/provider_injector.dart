import 'package:gain_study_case/src/resources/network_helper.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'provider_injector.config.dart';
import 'mock_client.dart';
import 'provider_test.dart';

final GetIt getIt = GetIt.instance;

@injectableInit
Future<void> configureMockInjection() async {
  //$initGetIt(getIt);

  getIt.registerSingleton<MockClient>(MockClient());

  getIt
      .registerSingleton<NetworkHelper>(NetworkHelper(getIt.get<MockClient>()));

  getIt.registerFactory(
      () => ProviderTest(getIt<NetworkHelper>(), getIt<MockClient>()));
}
