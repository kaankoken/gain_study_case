import 'dart:io';

import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;
import 'package:flutter_test/flutter_test.dart' as unit_test;
import 'package:flutter_test/flutter_test.dart';
import 'package:gain_study_case/src/blocs/movie/movie_bloc.dart';
import 'package:gain_study_case/src/blocs/movie/movie_list_bloc.dart';
import 'package:gain_study_case/src/di/bloc_injector.dart';
import 'package:gain_study_case/main.dart' as app;
import 'package:gain_study_case/src/models/state.dart';

Future main() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  unit_test.setUpAll(() {
    app.main();
  });
  await DotEnv.dotenv.load(fileName: ".env");

  //await configureInjection();

  unit_test.group(
    "BloC test",
    () {
      unit_test.test(
        "Movie List Bloc",
        () {
          var movieListBloc = getIt<MovieListBloc>();
          movieListBloc.init();
          movieListBloc.fetchMovieList.listen(
            unit_test.expectAsync1(
              (value) {
                unit_test.expect(
                  value,
                  unit_test.isInstanceOf<SuccessState>(),
                );
              },
            ),
          );
          movieListBloc.getMovieList({"page": "1"});
          sleep(Duration(seconds: 1));
        },
      );

      unit_test.test(
        "Movie Detail Bloc",
        () {
          var movieDetail = getIt<MovieBloc>();
          movieDetail.init();
          movieDetail.fetchMovieDetail.listen(
            unit_test.expectAsync1(
              (value) {
                unit_test.expect(
                  value,
                  unit_test.isInstanceOf<SuccessState>(),
                );
              },
            ),
          );
          movieDetail.getMovieDetail({"movie_id": "337404"});
        },
      );
    },
  );
}
